from flask import Flask, url_for, render_template, request

app = Flask(__name__)

@app.route("/",methods = ['GET','POST'])
def index():
  '''
  Handles basic gets/posts
  '''
  if request.method == 'POST':
    return 'OK'
  else:
    return render_template('image.html')



if __name__ == "__main__":
  '''
  Runs server if main
  '''
  app.run(host='0.0.0.0', port=80, debug=True)
 
