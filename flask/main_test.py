from flask import Flask, url_for, render_template, request
import datetime
app = Flask(__name__)

@app.route("/", methods=['GET','POST'])
def hello():
  if request.method == 'POST':
    print request.form
    return 'ok'
  else:
    now = datetime.datetime.now()
    timeString = now.strftime("%Y-%m-%d %H:%M")
    templateData ={
          'title' : 'HELLO!',
          'time': timeString}
    return render_template('main.html', **templateData)

if __name__ == "__main__":
  app.run(host='0.0.0.0', port=8080, debug=True)
