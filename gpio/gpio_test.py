import RPi.GPIO as GPIO
import time

left_led = 7
right_led = 11

GPIO.setmode(GPIO.BOARD)

GPIO.setup(left_led, GPIO.OUT,1)
GPIO.setup(right_led, GPIO.OUT,1)

GPIO.output(left_led,1)
time.sleep(2)
GPIO.output(left_led,0)
GPIO.output(right_led,1)
time.sleep(2)
GPIO.output(right_led,0)


GPIO.cleanup()
