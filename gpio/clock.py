import urllib
import RPi.GPIO as GPIO
import time
import sys
import signal
import datetime

bit0 = 7
bit1 = 11
bit2 = 12
bit3 = 15
pm_bit= 16

#url = "http://192.168.11.8"

def setUpGPIO():
  GPIO.setmode(GPIO.BOARD)

  GPIO.setup(bit0, GPIO.OUT,0)
  GPIO.setup(bit1, GPIO.OUT,0)
  GPIO.setup(bit2, GPIO.OUT,0)
  GPIO.setup(bit3, GPIO.OUT,0)
  GPIO.setup(pm_bit, GPIO.OUT,0)

def setPins(time):
  if (time%12)%2//1 == 1:
    GPIO.output(bit0,1)
  else:
    GPIO.output(bit0,0)
  
  if (time%12)%4//2 == 1:
    GPIO.output(bit1,1)
  else:
    GPIO.output(bit1,0)    

  if (time%12)%8//4 == 1:
    GPIO.output(bit2,1)
  else:
    GPIO.output(bit2,0)    

  if (time%12)%16//8 == 1:
    GPIO.output(bit3,1)
  else:
    GPIO.output(bit3,0)    

  if time//12 == 1:
    GPIO.output(pm_bit,1)
  else:
    GPIO.output(pm_bit,0)

def getTime():
  '''
  sock = urllib.urlopen(url)
  htmlSource = sock.read()
  sock.close()
  splitSource = htmlSource.split('\n')
  stripStr = splitSource[5].split(' ')
  stripStr2 = stripStr[2].split(':')
  return int(stripStr2[0])
  '''
  now = datetime.datetime.now()
  #print now.hour
  return now.hour


def signal_handler(signal,frame):
  print "\nCtrl+C Pressed"
  onExit()

def onExit():
  GPIO.cleanup()
  sys.exit(0)

def main():
  setUpGPIO()
  signal.signal(signal.SIGINT, signal_handler)
  while True:
    t = getTime()
    setPins(t)
    time.sleep(1)


if __name__ == "__main__":
  main()
