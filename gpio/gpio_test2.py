import RPi.GPIO as GPIO
import time

bit0 = 7
bit1 = 11
bit2 = 12
bit3 = 15
pm_bit= 16

GPIO.setmode(GPIO.BOARD)

GPIO.setup(bit0, GPIO.OUT,0)
GPIO.setup(bit1, GPIO.OUT,0)
GPIO.setup(bit2, GPIO.OUT,0)
GPIO.setup(bit3, GPIO.OUT,0)
GPIO.setup(pm_bit, GPIO.OUT,0)

GPIO.output(bit0,1)
time.sleep(1)
GPIO.output(bit0,0)

GPIO.output(bit1,1)
time.sleep(1)
GPIO.output(bit1,0)

GPIO.output(bit2,1)
time.sleep(1)
GPIO.output(bit2,0)

GPIO.output(bit3,1)
time.sleep(1)
GPIO.output(bit3,0)

GPIO.output(pm_bit,1)
time.sleep(1)
GPIO.output(pm_bit,0)


GPIO.cleanup()
